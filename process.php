<!-- This isnt View. its just gonna do redirect to the next question or the final page 
    process our answears, and if it is right - add it to our score var/
	Score is gonna be added to php SESSION - global var that u can use anywhere in your script.
	We will create one SESSION var called SCORE and we add to it as user get right answear
	-->
	
<?php include 'database.php'; ?>
<?php session_start(); ?>	<!--this has to go on every page where u gonna use the SESSION var(on top of the page along with database include)-->
<?php


	//Check to see if score is set_error_handler
	//if(!isset($_SESSION['score'])){
	//	$_SESSION['score'] = 0;

        //if score var isnt there - we wonna create it but we 0 score, and as user answear it will be added to that global
	//}

	//Get the user answear
	//so that when the user answear it is submitted to this file
	if($_POST){
	//at this point we wonna get 2 things: question number and selected choice
	//number is in URL and i wonna pass it through POST and not GET(not through the URL) 
	//so we go to the question form and create a hidden input
		$number = $_POST['number'];
		$selected_choice = $_POST['choice'];
		//echo $number.'<br>'.$selected_choice;  check so we can track user choice 
		
		//now we need logic to redirect user to next question
		$next = $number+1;
		
		//next we need another query in order to get right answear to that question
		
		/*
		 * Get total questions
		 */
		$query = "SELECT * FROM questions";
		$result = $mysqli->query($query) or die($mysqli->error.__LINE__);
		$total =  $result->num_rows;
		
		/*
		 * Get correct choice
		 */
		$query = "SELECT * FROM choices WHERE question_number=$number AND is_correct = 1";
		$result = $mysqli->query($query) or die($mysqli->error.__LINE__);
		//print_r($result);	and we receive Object: mysqli_result Object ( [current_field] => 0 [field_count] => 4 [lengths] => [num_rows] => 1 [type] => 0 ) 
		
		
		//Get row
		$row = $result->fetch_assoc();
		//print_r($row);  and we receive whole row of a table in Array: Array ( [id] => 3 [question_number] => 1 [is_correct] => 1 [text] => Require exits the script if it cannot find the file to include ) 
		
		//Set the correct choice
		$correct_choice = $row['id'];

        if(!isset($_SESSION['score'])){
            $_SESSION['score'] = 0;
            
        } elseif($correct_choice == $selected_choice){
            //Correct answear
            $_SESSION['score']++;
        }  
		//!!!!!!!!!!!!!!!! INCORRECT score
		
		// Check if last question or go to final page
		if($number == $total){
			header('Location: final.php');	//redirect function
			exit();
		}else {
			header('Location: question.php?n='.$next);
		}

	}

?>