<!-- in this program we can add info to DB through the browser as opposed to just manually sticking it in DB
 In DB's table there is no id field that increments automatycally 
 In this case we want each question to be bound by the question number 
 that means question 2 should always be q.2. And if we do an autoincrement and then delete a queation, things will get messed up
 
-->
<?php include 'database.php'; ?>
<?php
	/*
	 * Get Total Questions
	 */
	$query = "SELECT * FROM questions";
	// Get Results
	$result = $mysqli->query($query) or die($mysqli->error.__LINE__);	//OBJECT
	$total = $result->num_rows;
	
 ?>
<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8" />
		<title>PHP Quizzer!</title>
		<link rel="stylesheet" href="css/style.css" type="text/css" />
	</head>
	<body>
		<header>
			<div class="container">
				<h1>PHP Quizzer</h1>
			</div>
		</header>
		<main>
			<div class="container">
				<h2> Test Your PHP Knowledge</h2>
				<p>This is a multiple choice quizz to test your knowledge of PHP </p>
				<ul>
					<li><strong>Number of Questions: </strong><?php echo $total; ?></li>
					<li><strong>Type: </strong>Multiple Choice</li>
					<li><strong>Estimated Time: </strong><?php echo $total * .5; ?> Minutes</li>
				</ul>
				<a href="question.php?n=1" class="start">Start Quizz</a>	<!--because we want to make sure we go to the 1st question. 'n' in URL stands for number -->
			</div>
		</main>
		<footer>
			<div class="container">
				Copyright &copy; 2015, PHP Quizzer
			</div>
		</footer>
	
	</body>
</html>