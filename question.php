<!-- We copy from index.php because we want the same layout--> 
<?php include 'database.php'; ?>
<?php session_start(); ?>
<?php
// Here is all our query logic
// When working on a production app u usually wonna have a separate page for your queries or follow MVC pattern especially if it is page to display
// Instead we can create a DB class and will use it as kind of an abstraction layer to communicate with DB

// First we wonna get the number of the question by using superglobal GET 
// which make 'n' from URL accessible to us to use inside queries

//so Set question number
	$number = (int) $_GET['n'];	//typecast it to be an integer
	
	
// our first query

	/*
	 * Get Question
	 */
	 
	//we want get just one question from table because this is just one-question page
	// select all from questions where the question number equels 'n' which is set in $number
	
	$query = "SELECT * FROM questions WHERE question_number = $number"; 	//we havent ran anything yet? we just put query into var
	//Get Result
	$result = $mysqli->query($query) or die($mysqli->error.__LINE__); //query() is actually method that run our query. LINE give us an informative error if sth goes wrong
	
	//the last thing - var that we wonna use
	$question = $result->fetch_assoc(); //-> associative array of data that we requested and we can use it down here
	
	//choices are coming from the questions table so we need separate query
	//all choices for one question so we gonna get multiple values
	// and we gonna asign it to a var and loop over it
	
	//Get Choices 
	$query_for_choices = "SELECT * FROM choices WHERE question_number = $number";
	//Get Results
	$choices = $mysqli->query($query_for_choices) or die($mysqli->error.__LINE__);
	
	// Get total questions 
	$query = "SELECT * FROM questions";
	$result = $mysqli->query($query) or die($mysqli->error.__LINE__);
	$total = $result->num_rows;
	
	?>
<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8" />
		<title>PHP Quizzer!</title>
		<link rel="stylesheet" href="css/style.css" type="text/css" />
	</head>
	<body>
		<header>
			<div class="container">
				<h1>PHP Quizzer</h1>
			</div>
		</header>
		<main>
			<div class="container">
				<div class="current">Question <?php echo $question['question_number']; ?> of <?php echo $total; ?></div>
				<p class="question">
					<?php echo $question['text']; ?>
				</p>
				<form method="POST" action="process.php">
					<ul class="choices">
					<?php while($row = $choices->fetch_assoc()) : ?>
						<li><input name="choice" type="radio" value="<?php echo $row['id']; ?>" /> <?php echo $row['text']; ?> </li>
					<?php endwhile; ?>
					</ul>
					<!--<p>Current Score: <?php echo $_SESSION['score'] ?></p>-->
					<input type="submit" value="submit" />
					<input type="hidden" name="number" value="<?php echo $number; ?>" />
				</form>
			</div>
		</main>
		<footer>
			<div class="container">
				Copyright &copy; 2015, PHP Quizzer
			</div>
		</footer>
	
	</body>
</html>

