<!-- Its a form where we add quizzes to the DB through the browser--> 
<!-- in this program we can add info to DB through the browser as opposed to just manually sticking it in DB
 In DB's table there is no id field that increments automatycally 
 In this case we want each question to be bound by the question number 
 that means question 2 should always be q.2. And if we do an autoincrement and then delete a queation, things will get messed up
 
-->
<?php include 'database.php'; ?>
<?php 
	//what ever we gonna do only work if submit is clicked
	if(isset($_POST['submit'])){
		//Get POST vars
		$question_number = $_POST['question_number'];
		$question_text = $_POST['question_text'];
		$correct_choice = $_POST['correct_choice'];
		//Create choices array
		$choices = array();
		$choices[1] = $_POST['choice1'];
		$choices[2] = $_POST['choice2'];
		$choices[3] = $_POST['choice3'];
		$choices[4] = $_POST['choice4'];
		$choices[5] = $_POST['choice5'];
		
		//Question query
		$query = "INSERT INTO questions (question_number, text) VALUES ('$question_number', '$question_text')";
		$insert_row = $mysqli->query($query) or die($mysqli->error.__LINE__);
		//we havent inserted choices yet 
		//Validate insert
		if($insert_row){		//means if the question query is successful
			foreach($choices as $choice=>$value){
				if($value != ''){
					if($correct_choice == $choice){
						$correct_choice = 1;
					} else {
						$correct_choice = 0;
					}
					//Choice query
					$query = "INSERT INTO choices (question_number, is_correct, text) VALUES ('$question_number', '$correct_choice', '$value')";
					$insert_row = $mysqli->query($query) or die($mysqli->error.__LINE__);
					//Validate insert 
					if($insert_row){
						continue;
					} else {
						die('Error : ('.$mysqli->errno .') '.$mysqli->error);
					}
				}
			}
			$msg = 'Question has been added';
			
		}
	}	
	$query = "SELECT * FROM questions";
	$questions = $mysqli->query($query) or die($mysqli->error.__LINE__);
	$total = $questions->num_rows;
	$next = $total + 1;
?>
<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8" />
		<title>PHP Quizzer!</title>
		<link rel="stylesheet" href="css/style.css" type="text/css" />
	</head>
	<body>
		<header>
			<div class="container">
				<h1>PHP Quizzer</h1>
			</div>
		</header>
		<main>
			<div class="container">
				<h2>Add A Question!</h2>
				<?php 
					if(isset($msg)){
						echo '<p>'.$msg.'</p>';
					}
				?>
				<form method="POST" action="add.php">
					<p>
						<label>Question Number: </label>
						<input type="number" name="question_number" value="<?php echo $next; ?>"/>
					</p>
					<p>
						<label>Question Text: </label>
						<input type="text" name="question_text" />
					</p>
					<p>
						<label>Choice #1: </label>
						<input type="text" name="choice1" />
					</p>
					<p>
						<label>Choice #2: </label>
						<input type="text" name="choice2" />
					</p>
					<p>
						<label>Choice #3: </label>
						<input type="text" name="choice3" />
					</p>
					<p>
						<label>Choice #4: </label>
						<input type="text" name="choice4" />
					</p>
					<p>
						<label>Choice #5: </label>
						<input type="text" name="choice5" />
					</p>
					<p>
						<label>Correct Choice Number: </label>
						<input type="number" name="correct_choice" />
					</p>
					<input type="submit" name="submit" value="Submit" />
				</form>
			</div>
		</main>
		<footer>
			<div class="container">
				Copyright &copy; 2015, PHP Quizzer
			</div>
		</footer>
	
	</body>
</html>