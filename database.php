<!-- its our DB connection file:
	- hold connection parameters
	- connection through mysqli API (mysql is depricated and not really safe/PDO is more advanced)
This example isnt the best practice, just to make it understand
	-->
	
	
<?php
	//Create connection credentials(like host, user, password..)
	$db_host = 'localhost';
	$db_user = 'root';
	$db_pass = 'moschino28';
	$db_name = 'quizzer';

	//Create mysqli object

	//when dealing with mysqli there are procedure or ob-oriented ways
	// procedural: $mysqli_error
	// oop: 	   $mysqli->error
	$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_name);	//this particular way

	//SimpleError handler
	if($mysqli->connect_error){
		printf("Connect failed: %s\n", $mysqli->connect_error);
		exit();
	}
	//This is our entire DB file.
	//we wonna have to include this on every page when we want to use DB
	//include is really easy
	//so We are connected tp our DB and next we are ready to start running queries
?>



 <!-- Sometimes it is recommended no to use close tag 
especially if its the end of the file perhaps cause then it will be HTML embed-->