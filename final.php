<!-- The View when you complite the test and it tells congratulations, score.. -->
<!-- Copy&Paste question.php 
	When you get into bigger apps and more advanced staff u dont want to have
	DOCTYPE and all head staff in every page. Generally u will create header.php/footer.php 
-->


<!-- record score is very simple - just start SESSION and output the session var-->
<?php session_start(); ?>
<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8" />
		<title>PHP Quizzer!</title>
		<link rel="stylesheet" href="css/style.css" type="text/css" />
	</head>
	<body>
		<header>
			<div class="container">
				<h1>PHP Quizzer</h1>
			</div>
		</header>
		<main>
			<div class="container">
				<h2>You're Done!</h2>
				<p>Congrates! You have completed the test</p>
				
				<p>Final Score: <?php echo $_SESSION['score']; ?></p>
				<a href="question.php?n=1" class="start">Take Again!</a>
			</div>
		</main>
		<footer>
			<div class="container">
				Copyright &copy; 2015, PHP Quizzer
			</div>
		</footer>
	
	</body>
</html>
